import Head from 'next/head';
import React from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import Slider from "react-slick";
import Hero from '../components/Hero'

import "../styles.scss"

class Home extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        trendingFilter: null,
      }
  }
  render(){

    const settings = {
      dots: true,
      arrows:false,
      infinite: true,
      fade: true,
      speed: 750,
      autoplay: true,
      autoplaySpeed: 6000,
    };

    return(
      <React.Fragment>
        <Head>
            <title>Movie Database | Love Film, TV and Cinema</title>
            <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        </Head>
        <Slider {...settings}>
        {this.props.all
          .slice(0, 6)
          .map((show, i) => {
          return(
          <section key={i}>
            <Hero 
              key={show.id}
              title={show.media_type === 'movie' ? show.title : show.name}
              backgroundImage={show.backdrop_path}
              date={show.media_type === 'movie' ? show.release_date : show.first_air_date}
              genres={show.genre_ids}
              videos=""
              userRating={show.vote_average * 10}
              />
          </section>
          )
        })}
        </Slider>
      </React.Fragment>
    )
  }
}

Home.getInitialProps = async function() {
  const res = await fetch('https://api.themoviedb.org/3/trending/all/week?api_key=a5ccc05c247dfd2c49d147b2a043eafa');

  const data = await res.json();

  return {
    all: data.results,
  };
};

export default Home;
