import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

const errorPage = () => (
	<div>
		<h1>oops, something went wrong</h1>
		<p> Return <Link href="/"><a>Home</a></Link></p>
	</div>
);

export default errorPage;