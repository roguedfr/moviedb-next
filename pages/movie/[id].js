import React from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import "../../styles.scss"

import Hero from '../../components/Hero'

const Movie = props => (
  <React.Fragment>
	<Hero 
    key={props.movie.id}
		title={props.movie.title}
		backgroundImage={props.movie.backdrop_path}
		date={props.movie.release_date}
		genres={props.movie.genres}
		videos={props.movieVideos}
    userRating={props.movie.vote_average * 10}
		/>
    <section className="pt5 pb5">
      <div className="grid-container">
        <div className="section section__dark grid-x grid-padding-x align-stretch">
          <div className="cell small-5 background__cover background__center" style={{backgroundImage:'url(https://image.tmdb.org/t/p/original/'+ props.movie.poster_path +')'}}></div>
          <div className="cell small-7 pb3 pt3">
            <div className="pl3 pr3 grid-x grid-margin-x">
              <div className="small-9 cell">
                <h3 className="heading heading__light heading__h3 heading__caps">{props.movie.title}</h3>
              </div>
              <div className="small-3 cell">

              </div>
              <div className="pt2 border__bottom border__secondary border__thick small-12 cell">
                <h5 className="heading heading__caps heading__h5 heading__light">
                  Synopsis
                </h5>
              </div>
              <div className="pt1 heading__light small-12 cell">
                {props.movie.overview}
              </div>
              <div className="pt1 heading__light small-7 grid-x cell">
                <div className="small-6 cell">
                  <span className="heading__light heading__h6 font700">Director</span>
                </div>
                <div className="small-6 cell">
                  <span className="heading__light heading__h6">{props.movieCredits.title}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>		
  </React.Fragment>
);

Movie.getInitialProps = async function(context) {
  const { id } = context.query;
  const res = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=a5ccc05c247dfd2c49d147b2a043eafa`);
  const resVideo = await fetch(`https://api.themoviedb.org/3/movie/${id}/videos?api_key=a5ccc05c247dfd2c49d147b2a043eafa`);
  const resCredits = await fetch(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=a5ccc05c247dfd2c49d147b2a043eafa`);
  const movie = await res.json();
  const movieVideos = await resVideo.json();
  const movieCredits = await resCredits.json();

  return { movie, movieVideos, movieCredits };
};


export default Movie;