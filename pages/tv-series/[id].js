import React from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import "../../styles.scss"

import Hero from '../../components/Hero'

const Movie = props => (
  <React.Fragment>
	<Hero
		title={props.movie.name}
		backgroundImage={props.movie.backdrop_path}
		date={props.movie.first_air_date}
		genres={props.movie.genres}
		videos={props.movieVideos}
    userRating={props.movie.vote_average * 10}
		/>			
  </React.Fragment>
);

Movie.getInitialProps = async function(context) {
  const { id } = context.query;
  const res = await fetch(`https://api.themoviedb.org/3/tv/${id}?api_key=a5ccc05c247dfd2c49d147b2a043eafa`);
  const resVideo = await fetch(`https://api.themoviedb.org/3/tv/${id}/videos?api_key=a5ccc05c247dfd2c49d147b2a043eafa`);
  const movie = await res.json();
  const movieVideos = await resVideo.json();

  return { movie, movieVideos };
};


export default Movie;