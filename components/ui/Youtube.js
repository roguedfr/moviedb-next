import React, {Component} from 'react';
import YouTube from 'react-player';

class youTube extends Component {

	render(){

	let pauseVideo = this.props.show;

		return(
			<div className='responsive-embed widescreen' style={{width: this.props.Width}}>
				<YouTube
					url = {'https://www.youtube.com/watch?v=' + this.props.Id}
					playing = {pauseVideo}
					muted = {!pauseVideo}
					controls =  'true'
				/>
			</div>
		)
	}
}

export default youTube