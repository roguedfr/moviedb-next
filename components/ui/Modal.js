import React, {Component} from 'react';
import Backdrop from './Backdrop';

class modal extends Component {

	shouldComponentUpdate(nextProps, nextState){
		return nextProps.show !== this.props.show;
	}

	render (){
		return (
			<React.Fragment>
				<Backdrop show={this.props.show} clicked={this.props.modalClosed}/>
				<div
					className="Modal"
					style={{
						visibility: this.props.show ? 'visible' : 'hidden',
						opacity: this.props.show ? '1' : '0'
					}}>
					{this.props.children}
				</div>
			</React.Fragment>
		)
	}
};

export default modal;