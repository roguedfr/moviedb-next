import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import Modal from './ui/Modal';
import YouTube from './ui/Youtube';
import Progress from './ui/Progress';

class Hero extends Component {

	state = {
		showTrailer: false,
		playTrailer: false
	}

	showTrailerHandler = (event) => {
		this.setState({showTrailer: true, playTrailer: true});
	}

	onTrailerDeactivate = (event) => {
		this.setState({showTrailer: false, playTrailer: false});
	}

	render(){
		return(
			<React.Fragment>
			    <Modal show={this.state.showTrailer} modalClosed={this.onTrailerDeactivate}>
					{this.props.videos.results ? this.props.videos.results.slice(-1).map(videoKey => {
						return <YouTube 
							Width='65vw'
							Id={videoKey.key}
							AutoPlay="true"
							show={this.state.showTrailer}/>
					}) : null}
				</Modal>
			  	<section className="grid-x grid-padding-x align-stretch h90 background__cover background__center overlay__dark-gradient" style={{backgroundImage:'url(https://image.tmdb.org/t/p/original/'+ this.props.backgroundImage +')'}}>
					<div className="grid-container small-12 cell grid-x">
						<div className="grid-x grid-padding-x small-12 align-bottom">
							<div className="small-8 cell pb7">
								<div className="heading__light heading__h5 genres">
									{this.props.genres ? this.props.genres.map(genKey => {
										return <span key={genKey.id}>{genKey.name}</span>
									}) : null}
								</div>
								<h1 className="heading__light heading__caps heading__h1 heading__inline heading__no-line-height pb0">
			  						{this.props.title}&nbsp;
			  					</h1>
			  					<h3 className="heading__inline heading__light heading__h3">
			  						({this.props.date.substring(0,4)})
			  					</h3>
			  					<div>
			  						{this.props.videos.results ? 
			  						<button onClick={this.showTrailerHandler}>
			  							<FontAwesomeIcon icon={faPlay} className="heading__primary heading__h5 trailerButton"/><span className="heading__light pl1 heading__h5">Watch Trailer</span>
			  						</button> : null}
			  					</div>
							</div>
							<div className="small-4 cell pb5">
								<Progress radius={70} stroke={5} progress={this.props.userRating} displayNumber={true} title="User Score"/>
							</div>
						</div>
					</div>
				</section>
			  </React.Fragment>
		)
	}
}

export default Hero;